#pragma once
#include "Arrow.h"

class Crossbow :public Object
{
	enum class Animate {
		NotLoad,
		Loaded,
		Attack,
		Start,
		End,

	};
	struct AnimationSwitch {
		Animate anim;
		int counter;
		int width;
		int hight;
		int shiftTop;
	};
public:
	Crossbow(std::function<Arrow* ()> newArrow);
	~Crossbow() override;
	void draw(sf::RenderWindow& wd) override;
	void eventUser(sf::Event& ev) override;
	void onRemove() override;

private:
	AnimationSwitch animation;
	std::function<Arrow* ()> m_newArrow;
	sf::Sprite m_CrossbowSprite;
	sf::Texture m_CrossbowTexture;
	sf::Sprite m_BasketSprite;
	sf::Texture m_BasketTexture;
	Arrow* m_arrow = NULL;
};

