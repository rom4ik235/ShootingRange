#include <iostream>

#include "CloudBackground.h"

float m_rand(const int max, const int min) {
	float value = (rand() % (max - min));
	return value;
}

CloudBackground::CloudBackground() : Object("CloudBackground"), cloudGenerator(&CloudBackground::createCloud, this)
{
	if (!m_fonTexture.loadFromFile("../assets/image/Fon.jpg")) {
		std::cout << "Asset not found!" << std::endl;
	}
	if (!m_CloudTexture.loadFromFile("../assets/image/Clouds.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_fonSprite.setTexture(m_fonTexture);
	if (!music.openFromFile("../assets/sound/fonovaya.wav"))
	{
		std::cout << "Asset not found!" << std::endl;
	}
	music.setPitch(2);
	music.setVolume(50);
	music.setLoop(true);
	music.play();
}

CloudBackground::~CloudBackground()
{
	isGenerate = false;
	cloudGenerator.detach();
	std::cout << "~CloudBackground!" << std::endl;
}

void CloudBackground::draw(sf::RenderWindow& wd)
{

	wd.draw(m_fonSprite);
	for (auto& cloud : clouds) {
		cloud.sprite.move({ cloud.speed ,0 });
		wd.draw(cloud.sprite);
	}
}

void CloudBackground::createCloud()
{
	while (isGenerate)
	{
		if (clouds.size() < 10) {
			bool sign = m_rand(3, 1);
			Cloud newCloud = { sf::Sprite(), sign ? 1.f : -1.f };
			newCloud.sprite.setTexture(m_CloudTexture);
			newCloud.sprite.setTextureRect(sf::IntRect(0, m_rand(6, 2) * 142, 282, 142));
			newCloud.sprite.setPosition({ sign ? -282.f : 800.f,m_rand(500,-50) });
			clouds.push_back(newCloud);
		}
		for (auto& cloud : clouds) {
			if (cloud.sprite.getPosition().x < -282 || cloud.sprite.getPosition().x > 800) {
				bool sign = m_rand(3, 1);
				cloud.speed = sign ? 1.f : -1.f;
				cloud.sprite.setTexture(m_CloudTexture);
				cloud.sprite.setTextureRect(sf::IntRect(0, m_rand(6, 2) * 142, 282, 142));
				cloud.sprite.setPosition({ sign ? -282.f : 800.f,m_rand(500,-50) });
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	}

}


