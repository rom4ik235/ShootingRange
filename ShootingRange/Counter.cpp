#include "Counter.h"

Counter::Counter(sf::Vector2f pos, int max, std::function<void(void)> onEnd) :
	Object("Counter"),
	m_max(max),
	m_onEnd(onEnd)
{
	if (!m_font.loadFromFile(static_cast<std::string>("../assets/Font/PixelFont.ttf"))) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_text.setFont(m_font);
	m_text.setCharacterSize(20);
	m_text.setStyle(sf::Text::Style::Regular);
	m_text.setFillColor(sf::Color::White);
	m_text.setString(std::to_string(count) + "/" + std::to_string(max));
	m_text.setPosition(pos);
}

Counter::~Counter()
{
}

bool Counter::addAndCheck()
{
	count++;
	m_text.setString(std::to_string(count) + "/" + std::to_string(m_max));
	if (count == m_max)
		return true;
	return false;
}

void Counter::draw(sf::RenderWindow& wd)
{
	wd.draw(m_text);
}
