#pragma once
#include "Object.h"
#include <thread>
#include <functional>

class Timer : public Object
{
public:
	Timer(sf::Vector2f pos, int m, int s, std::function<void(void)> onEndTimer);
	~Timer() override;

	void draw(sf::RenderWindow& wd) override;
	void counter();

private:
	std::function<void(void)> m_onEndTimer;
	int m_m, m_s;
	std::thread timer;
	sf::Font m_font;
	sf::Text m_text;
	bool still�ount = true;
};

