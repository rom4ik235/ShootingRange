#include "Audio.h"

static Audio* me;

Audio::Audio()
{
	if (deadBall.loadFromFile("../assets/sound/DeadBall.wav"))
		sounds[Track::DeadBall].setBuffer(deadBall);
	if (click.loadFromFile("../assets/sound/click.wav"))
		sounds[Track::Click].setBuffer(click);
	if (dmgNLO.loadFromFile("../assets/sound/dmg_nlo.wav"))
		sounds[Track::DMG_NLO].setBuffer(dmgNLO);
	if (telepot_NLO.loadFromFile("../assets/sound/teleport.wav"))
		sounds[Track::Telepot_NLO].setBuffer(telepot_NLO);
}

Audio* Audio::getThis()
{
	if (me == nullptr) {
		me = new Audio();
	}
	return me;
}

void Audio::play(Track t)
{
	sounds[t].play();
}
