#include "Scene.h"

#include <iostream>

#include "GameManager.h"
#include "CloudBackground.h"
#include "Button.h"
#include "DecorTower.h"

void Scene::menu() {
	GameManager* manager = GameManager::getThis();
	manager->clear(0b1);
	Button* startEndless = new Button({ 20,550 }, L"����������");
	startEndless->onPress = []() {
		Scene::start();
		return true;
	};
	Button* startGame = new Button({ 20,500 }, L"����� ����");
	startGame->onPress = []() {
		Scene::foreword();
		return true;
	};
	manager->addObject(startEndless, 2);
	manager->addObject(startGame, 2);
	manager->addObject(new DecorTower(), 1);
}

void Scene::fon() {
	GameManager* manager = GameManager::getThis();
	CloudBackground* fon = new CloudBackground();
	manager->addObject(fon, 0);
}