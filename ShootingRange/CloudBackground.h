#ifndef CLOUDBACKGROUND_H
#define CLOUDBACKGROUND_H
#include "Object.h"
#include <thread>

class CloudBackground : public Object
{
	struct Cloud
	{
		sf::Sprite sprite;
		float speed;
	};
public:
	CloudBackground();
	~CloudBackground();
	void draw(sf::RenderWindow& wd) override;

private:
	void createCloud();
	sf::Music music;
	std::thread cloudGenerator;
	bool isGenerate = true;
	sf::Sprite m_fonSprite;
	sf::Texture m_fonTexture;
	sf::Texture m_CloudTexture;
	std::vector<Cloud> clouds;
};

#endif ///CLOUDBACKGROUND_H
