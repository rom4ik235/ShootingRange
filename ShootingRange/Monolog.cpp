#include "Monolog.h"

Monolog::Monolog(sf::Vector2f pos, const std::wstring& monolog, std::function<void(void)> onEndMono) :
	Object("Monolog"),
	m_monolog(monolog),
	m_onEndMono(onEndMono)
{
	if (!m_font.loadFromFile(static_cast<std::string>("../assets/Font/PixelFont.ttf"))) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_text.setFont(m_font);
	m_text.setCharacterSize(20);
	m_text.setStyle(sf::Text::Style::Regular);
	m_text.setFillColor(sf::Color::White);
	m_text.setString("");
	m_text.setPosition(pos);
}

Monolog::~Monolog()
{
	std::cout << "~" << getName() << std::endl;
}

void Monolog::draw(sf::RenderWindow& wd)
{
	if (state == State::WriteMono) {
		if (count < m_monolog.size()) {
			count++;
			m_text.setString(m_monolog.substr(0, count));
		}
		else
		{
			state = State::EndMono;
			if (m_onEndMono != nullptr)
				m_onEndMono();
		}
	}
	wd.draw(m_text);
}

void Monolog::eventUser(sf::Event& ev)
{
	if (ev.type == sf::Event::MouseButtonPressed) {
		if (state == State::WriteMono) {
			m_text.setString(m_monolog);
			state = State::EndMono;
			if (m_onEndMono != nullptr)
				m_onEndMono();
		}
	}
}
