#pragma once
#include "Object.h"
#include <functional>
class Arrow : public Object
{
public:
	Arrow(std::function<void(float x, float y)> addEvent);
	~Arrow() override;
	void draw(sf::RenderWindow& wd) override;
	void setPosition(float x, float y = 450.f);
	void move(float y);

private:
	std::function<void(float x, float y)> m_addEvent;
	float target = -40;
	sf::Sprite m_ArrowSprite;
	sf::Texture m_ArrowTexture;
	sf::RectangleShape m_background;
};

