#pragma once

#include <SFML/Audio.hpp>
#include <iostream>
#include <thread>
#include <map>


class Audio
{
public:
	enum class Track
	{
		DeadBall,
		Click,
		DMG_NLO,
		Telepot_NLO,
	};
	Audio();
	static Audio* getThis();
	void play(Track t);

	Audio(Audio& other) = delete;
	void operator=(const Audio&) = delete;
private:
	sf::SoundBuffer deadBall;
	sf::SoundBuffer click;
	sf::SoundBuffer dmgNLO;
	sf::SoundBuffer telepot_NLO;
	std::map<Track, sf::Sound> sounds;
};

