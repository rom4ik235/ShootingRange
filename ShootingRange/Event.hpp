#ifndef GAME_EVENT_HPP
#define GAME_EVENT_HPP

namespace Game {
	class Event
	{
	public:
		Event(float x, float y) :pos({ x,y }), type(EventType::Attack) {}
		Event(long int id) :id(id), type(EventType::Dead) {}

		struct Position {
			float x;
			float y;
		};
		enum class EventType {
			Attack,
			Dead,
		};

		EventType type;

		union
		{
			Position pos;
			long int id;
		};
	};
}

#endif // EVENT_HPP
