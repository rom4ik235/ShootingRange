#include "Arrow.h"

Arrow::Arrow(std::function<void(float x, float y)> addEvent) :
	Object("Arrow"),
	m_addEvent(addEvent)
{
	if (!m_ArrowTexture.loadFromFile("../assets/image/Arrow.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_ArrowSprite.setTexture(m_ArrowTexture);
	m_ArrowSprite.setScale({ 4,4 });
	m_ArrowSprite.setOrigin({ 3,0 });
	m_background.setSize({ 10,10 });
	m_background.setFillColor(sf::Color::Red);
	m_background.setOrigin(5, 5);
}

Arrow::~Arrow()
{
	//std::cout << "~Arrow" << std::endl;
}

void Arrow::draw(sf::RenderWindow& wd)
{
	if (target > -40) {
		sf::Vector2f arrPos = m_ArrowSprite.getPosition();
		if (arrPos.y <= target) {
			onRemove();
			return;
		}
		float scale = ((arrPos.y - target) / (480 - target)) + 0.1;
		if (scale < 0.3 && scale > 0.2) {
			m_addEvent(arrPos.x, arrPos.y);
			m_background.setPosition(arrPos);
		}
		m_ArrowSprite.setScale({ scale * 4,scale * 4 });
		m_ArrowSprite.move({ 0,(-50 * scale) - 1 });
	}
	wd.draw(m_ArrowSprite);
	//wd.draw(m_background);
}


void Arrow::move(float y)
{
	target = y - 40;
}

void Arrow::setPosition(float x, float y)
{
	m_ArrowSprite.setPosition({ x,y });
}
