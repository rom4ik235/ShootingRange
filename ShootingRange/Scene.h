#pragma once

namespace Scene {
	void menu();
	void fon();

	void timeGame();
	void foreword();
	void missionComplete();
	void missionFail();
	void youWin();

	void start();
}