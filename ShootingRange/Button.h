#pragma once
#include <functional>
#include "Object.h"

class Button :public Object
{
	enum State
	{
		None,
		Focus,
		Active,
	};

public:
	Button(sf::Vector2f pos, const std::wstring title);
	~Button() override;
	void eventUser(sf::Event& ev) override;
	void draw(sf::RenderWindow& wd) override;
	std::function<bool(void)> onPress;
	//void (*onPress)(void);

private:
	State m_state = State::None;
	sf::Vector2f m_pos;
	sf::Font m_font;
	sf::Text m_text;
	sf::RectangleShape  m_background;
	sf::Color color_Active = sf::Color(32, 63, 110);
	sf::Color color_Focus = sf::Color(114, 168, 149);
	sf::Color color_None = sf::Color(37, 133, 99);
	sf::Color color_Text = sf::Color(20, 55, 130);

};