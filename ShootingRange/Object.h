#ifndef OBJECT_H
#define OBJECT_H

#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "Event.hpp"


class Object
{
public:
	Object(const std::string name = "HollowObject") :m_name(name) {}
	virtual ~Object() { }
	virtual void eventUser(sf::Event& ev);
	virtual void eventGame(Game::Event& ev);
	
	virtual void draw(sf::RenderWindow& wd);
	virtual void onRemove();
	void setID(long int _id) { m_id = _id; };
	long int getID() { return m_id; }
	std::string getName() { return m_name + std::to_string(m_id); }
	void remove() { m_isRemove = true; }
	bool isRemove() { return m_isRemove; }
private:
	long int m_id = -1;
	bool m_isRemove = false;
	const std::string m_name;
};
#endif //OBJECT_H
