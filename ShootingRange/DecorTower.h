#pragma once
#include "Object.h"
class DecorTower : public Object
{
	enum class Animate {
		None,
		Start,
		End,

	};
	struct AnimationSwitch {
		Animate anim;
		int counter;
	};
public:
	DecorTower();
	~DecorTower() override;
	void draw(sf::RenderWindow& wd) override;
	void onRemove() override;

private:
	AnimationSwitch animation;
	sf::Sprite m_TowerSprite;
	sf::Texture m_TowerTexture;
};

