#include "Timer.h"

Timer::Timer(sf::Vector2f pos, int m, int s, std::function<void(void)> onEndTimer) :
	Object("Timer"),
	m_m(m),
	m_s(s),
	m_onEndTimer(onEndTimer),
	timer(&Timer::counter, this)
{
	if (!m_font.loadFromFile(static_cast<std::string>("../assets/Font/PixelFont.ttf"))) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_text.setFont(m_font);
	m_text.setCharacterSize(30);
	m_text.setStyle(sf::Text::Style::Regular);
	m_text.setFillColor(sf::Color::White);
	m_text.setString("00:00");
	m_text.setPosition(pos);
}

Timer::~Timer()
{
	still�ount = false;
	timer.join();
	std::cout << "~" << getName() << std::endl;
}

void Timer::draw(sf::RenderWindow& wd)
{
	wd.draw(m_text);
}

void Timer::counter()
{
	long long startTime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	while (still�ount)
	{
		long long nowTime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		int m = (nowTime - startTime) / 60;
		int s = (nowTime - startTime) % 60;
		m_text.setString((m < 10 ? "0" : "") + std::to_string(m) + ":" + (s < 10 ? "0" : "") + std::to_string(s));
		if (s >= m_s && m >= m_m) {
			if (m_onEndTimer != nullptr) {
				m_onEndTimer();
			}
			return;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}
