#ifndef BALL_H
#define BALL_H
#include "Object.h"

class Ball : public Object
{
	enum class Animate {
		None = 0,
		Move,
		Dead,
	};
	struct AnimationSwitch {
		Animate anim;
		int counter;
		int width;
		int hight;
		int shiftTop;
	};
public:
	Ball(sf::Vector2f pos, float gain);
	~Ball() override;

	void draw(sf::RenderWindow& wd) override;
	void eventGame(Game::Event& ev) override;
	void onRemove() override;

private:
	sf::SoundBuffer buffer;
	AnimationSwitch animation;
	sf::Sprite m_BallSprite;
	sf::Texture m_BallTexture;
	sf::RectangleShape m_background;
	float m_gain;
};

#endif ///BALL_H
