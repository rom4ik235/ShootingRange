#include "NLO.h"
#include "Audio.h"
#include "GameManager.h"

NLO::NLO(sf::Vector2f pos) :Object("NLO"),
animation({ Animate::Start, 0, 364, 182, 0 })
{
	if (!m_NLOTexture.loadFromFile("../assets/image/nlo.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_NLOSprite.setTexture(m_NLOTexture);
	m_NLOSprite.setTextureRect(sf::IntRect(0, 0, 364, 182));
	m_NLOSprite.setOrigin({ 182,91 });
	m_NLOSprite.setScale({ 0.5,0.5 });
	m_NLOSprite.setPosition(pos);


	m_hitBox.setPosition(pos);
	m_hitBox.setFillColor(sf::Color(255, 0, 0, 100));
	m_hitBox.setSize({ 182,50 });
	m_hitBox.setOrigin(91, 5);

	m_hp.setPosition(pos);
	m_hp.setFillColor(sf::Color::Red);
	m_hp.setSize({ 100, 5 });
	m_hp.setOrigin(50, 55);
	Audio::getThis()->play(Audio::Track::Telepot_NLO);
	do
	{
		move.points.push_back({ (float)(rand() % (700 - 100)) + 1,(float)(rand() % (400 - 100)) + 1 });
	} while (move.points.size() < 3);
}

NLO::~NLO()
{
	//std::cout << "~" << getName() << std::endl;
}

void NLO::draw(sf::RenderWindow& wd)
{
	if (animation.anim == Animate::Move) {
		sf::Vector2f p = m_NLOSprite.getPosition();
		sf::Vector2f pos = move.points[move.counter];
		int distance = sqrt((pos.x - p.x) * (pos.x - p.x) + (pos.y - p.y) * (pos.y - p.y));
		if (distance > 5) {
			float x = move.speed * (pos.x - p.x) / distance;
			float y = move.speed * (pos.y - p.y) / distance;
			m_hp.move({ x,y });
			m_NLOSprite.move({ x,y });
			m_hitBox.move({ x,y });
		}
		else {
			move.counter++;
			if (move.counter >= move.points.size()) {
				move.counter = 0;
			}
		}
		wd.draw(m_hp);
	}
	else
	{

		if (animation.counter == 3 && animation.anim != Animate::End) {
			animation.anim = Animate::Move;
		}
		else if (animation.counter >= 11) {
			GameManager::getThis()->pushGameEvent(Game::Event(getID()));
			remove();
			return;
		}
		m_NLOSprite.setTextureRect(sf::IntRect(animation.counter * animation.width, 0, animation.width, animation.hight));
		animation.counter++;
	}
	wd.draw(m_NLOSprite);
}

void NLO::eventGame(Game::Event& ev)
{
	if (ev.type == Game::Event::EventType::Attack) {
		if (animation.anim == Animate::Move) {
			if (m_hitBox.getGlobalBounds().contains(sf::Vector2f(ev.pos.x, ev.pos.y))) {
				Audio::getThis()->play(Audio::Track::DMG_NLO);
				sf::Vector2f old = m_hp.getSize();
				float hp = old.x - 10;
				if (hp <= 0) {
					Audio::getThis()->play(Audio::Track::Telepot_NLO);
					animation.counter = 9;
					animation.anim = Animate::End;
				}
				else
				{
					animation.counter = 7 - (int)(hp / 20);
					m_NLOSprite.setTextureRect(sf::IntRect(animation.counter * animation.width, 0, animation.width, animation.hight));
				}
				m_hp.setSize({ hp, old.y });
			}
		}
	}
}

void NLO::onRemove()
{
	animation.anim = Animate::End;
}
