#pragma once
#include "Object.h"
#include <functional>
class Counter : public Object
{
public:
	Counter(sf::Vector2f pos, int max, std::function<void(void)> onEnd = NULL);
	~Counter() override;
	bool addAndCheck();
	int getCount() { return count; }
	void draw(sf::RenderWindow& wd) override;

private:
	int count = 0;
	std::function<void(void)> m_onEnd;
	int m_max;
	sf::Font m_font;
	sf::Text m_text;
};

