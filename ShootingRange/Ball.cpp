#include <iostream>
#include <thread>

#include "Ball.h"
#include "Audio.h"

Ball::Ball(sf::Vector2f pos, float gain) :
	Object("Ball"),
	m_gain(gain),
	animation({ Animate::Move,0,60,150,0 })
{
	if (!m_BallTexture.loadFromFile("../assets/image/BallW.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	const sf::Color color = sf::Color((rand() % (255 - 0)), (rand() % (255 - 0)), (rand() % (255 - 0)));
	m_BallSprite.setColor(color);
	m_BallSprite.setTexture(m_BallTexture);
	m_BallSprite.setOrigin({ 30,50 });
	m_BallSprite.setTextureRect(sf::IntRect(0, 0, animation.width, animation.hight));
	m_BallSprite.setPosition(pos);

	m_background.setPosition(pos);
	m_background.setSize({ 50,80 });
	m_background.setFillColor(color);
	m_background.setOrigin(25, 50);
}

Ball::~Ball()
{
	//std::cout << "~" << getName() << std::endl;
}

void Ball::onRemove()
{
	animation.anim = Animate::Dead;
	animation.counter = 0;
	animation.width = 120;
	animation.shiftTop = 150;
}

void Ball::draw(sf::RenderWindow& wd)
{
	if (m_BallSprite.getPosition().y < -50) {
		remove();
		return;
	}
	m_BallSprite.move({ 0,-m_gain });
	if (animation.counter < 5) {
		m_BallSprite.setTextureRect(sf::IntRect(animation.counter * animation.width, animation.shiftTop, animation.width, animation.hight));
		animation.counter++;
	}
	else {
		if (animation.anim == Animate::Dead) {
			remove();
		}
		else
		{
			animation.counter = 0;
		}
	}

	m_background.setPosition(m_BallSprite.getPosition());
	//wd.draw(m_background);
	wd.draw(m_BallSprite);
}


void Ball::eventGame(Game::Event& ev)
{
	if (ev.type == Game::Event::EventType::Attack) {
		if (animation.anim == Animate::Dead)
			return;
		if (m_background.getGlobalBounds().contains(sf::Vector2f(ev.pos.x, ev.pos.y))) {
			Audio::getThis()->play(Audio::Track::DeadBall);
			animation.anim = Animate::Dead;
			animation.counter = 0;
			animation.width = 120;
			animation.shiftTop = 150;
		}
	}
}


