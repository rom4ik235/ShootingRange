#include "GameManager.h"

static GameManager* me;

GameManager::GameManager() :thread(&GameManager::cleaner, this)
{
	if (!m_CursorTexture.loadFromFile("../assets/image/aim.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_CursorSprite.setTexture(m_CursorTexture);
	m_CursorSprite.setScale({ 0.5,0.5 });
	m_CursorSprite.setOrigin({ 50,50 });
}

void GameManager::clear(char skip_layer)
{
	for (int i = 0; i < 3; i++) {
		if (skip_layer & 1 << i)
			continue;
		auto& layer = layers[i];
		for (auto pair : layer) {
			pair.second->onRemove();
		}
	}
}

void GameManager::draw(sf::RenderWindow& wd)
{
	for (auto& layer : layers) {
		auto it = layer.begin();
		while (it != layer.end())
		{
			if (!it->second->isRemove()) {
				it->second->draw(wd);
				it++;
			}
			else {
				long int id = it->first;
				trash.push(it->second);
				it++;
				layer.erase(id);
			}
		}
		m_CursorSprite.setPosition(static_cast<sf::Vector2f> (sf::Mouse::getPosition(wd)));
		wd.draw(m_CursorSprite);
	}
}

void GameManager::eventUser(sf::Event& ev)
{
	for (auto& layer : layers) {
		for (auto it = layer.begin(); it != layer.end(); it++) {
			it->second->eventUser(ev);
		}
	}
}

void GameManager::eventGame()
{
	while (!gameEvents.empty())
	{
		Game::Event& ev = gameEvents.front();
		for (auto& layer : layers) {
			for (auto it = layer.begin(); it != layer.end(); it++) {
				it->second->eventGame(ev);
			}
		}
		gameEvents.pop();
	}
}

void GameManager::pushGameEvent(const Game::Event ev)
{
	gameEvents.push(ev);
}

GameManager* GameManager::getThis()
{
	if (me == nullptr) {
		me = new GameManager();
	}
	return me;
}

void GameManager::cleaner()
{
	while (isLaunchedGame)
	{
		while (!trash.empty())
		{
			trash.front()->~Object();
			trash.pop();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}
