#pragma once
#include "Object.h"
#include <functional>
class Monolog : public Object
{
	enum class State
	{
		WriteMono,
		EndMono,
	};
public:
	Monolog(sf::Vector2f pos, const std::wstring& monolog, std::function<void(void)> onEndMono);
	~Monolog() override;
	void draw(sf::RenderWindow& wd) override;
	void eventUser(sf::Event& ev) override;

private:
	std::function<void(void)> m_onEndMono;
	State state = State::WriteMono;
	int count = 1;
	const std::wstring m_monolog;
	sf::Font m_font;
	sf::Text m_text;
	sf::Text m_nextText;
};

