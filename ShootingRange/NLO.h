#ifndef NLO_H
#define NLO_H

#include "Object.h"

class NLO : public Object
{
	struct Move
	{
		std::vector<sf::Vector2f> points;
		float speed = 5;
		int counter = 0;
	};
	enum class Animate {
		Move,
		Start,
		End,

	};
	struct AnimationSwitch {
		Animate anim;
		int counter;
		int width;
		int hight;
		int shiftTop;
	};

public:
	NLO(sf::Vector2f pos);
	~NLO() override;

	void draw(sf::RenderWindow& wd) override;
	void eventGame(Game::Event& ev) override;
	void onRemove() override;

private:
	AnimationSwitch animation;
	Move move;
	sf::Sprite m_NLOSprite;
	sf::Texture m_NLOTexture;
	sf::RectangleShape m_hitBox;
	sf::RectangleShape m_hp;
};

#endif // NLO_H

