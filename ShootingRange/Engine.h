#ifndef ENGINE_H
#define ENGINE_H
#include "GameManager.h"

class Engine
{
public:
	Engine(sf::RenderWindow& wd);
	void run(sf::RenderWindow& wd);

private:
	GameManager* manager;
	sf::RenderWindow& m_wd;
};
#endif //ENGINE_H