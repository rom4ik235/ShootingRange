#include "DecorTower.h"

DecorTower::DecorTower() :Object("DecorTower"),
animation({ Animate::Start, 0 })
{
	if (!m_TowerTexture.loadFromFile("../assets/image/tower.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	m_TowerSprite.setTexture(m_TowerTexture);
	m_TowerSprite.setOrigin({ 260,287 });
	m_TowerSprite.setPosition({ 800,900 });
}

DecorTower::~DecorTower()
{
}

void DecorTower::draw(sf::RenderWindow& wd)
{
	if (animation.anim == Animate::Start) {
		if (m_TowerSprite.getPosition().y > 600)
			m_TowerSprite.move({ 0,-20 });
		else
			animation.anim = Animate::None;
	}
	else if (animation.anim == Animate::End)
	{
		if (m_TowerSprite.getPosition().y < 900)
			m_TowerSprite.move({ 0,20 });
		else
			remove();
	}
	wd.draw(m_TowerSprite);
}

void DecorTower::onRemove()
{
	animation.anim = Animate::End;
}
