#include "Scene.h"

#include <iostream>

#include "GameManager.h"
#include "CloudBackground.h"
#include "Button.h"
//#include "Ball.h"
#include "Crossbow.h"
#include "Arrow.h"
#include "NLO.h"
#include "Timer.h"
#include "Monolog.h"
#include "Counter.h"

class NLOSpawner :public Object {
public:
	NLOSpawner(GameManager* manager) :
		Object("NLOSpawner"),
		manager(manager),
		�ounter(new Counter({ 700,50 }, 20))
	{
		manager->addObject(�ounter, 2);
		do
		{
			id_nlo.push_back(manager->addObject(new NLO({ m_rand(700,100), m_rand(500,100) }), 1));
		} while (id_nlo.size() < 3);

		manager->addObject(new Timer({ 700,20 }, 1, 0, [this]() {
			if (�ounter->getCount() == 0)
				Scene::youWin();
			else
				Scene::missionFail();
			}), 2);
	}
	~NLOSpawner() {
		std::cout << "~" << getName() << std::endl;
	}

	void eventGame(Game::Event& ev) override {
		if (ev.type == Game::Event::EventType::Dead) {
			auto it = std::find(id_nlo.begin(), id_nlo.end(), ev.id);
			if (it != id_nlo.end()) {
				if (�ounter->addAndCheck()) {
					Scene::missionComplete();
					return;
				}
				id_nlo.erase(it);
				NLO* nlo = new NLO({ m_rand(100,700),m_rand(100,500) });
				id_nlo.push_back(manager->addObject(nlo, 1));
			}
		}
	}

private:
	float m_rand(const int max, const int min) {
		float value = (rand() % (max - min)) + 1;
		return value;
	}
	std::vector<long int> id_nlo;
	Counter* �ounter;
	GameManager* manager;
};


void Scene::timeGame() {

	GameManager* manager = GameManager::getThis();

	manager->clear(0b1);
	Button* startBut = new Button({ 20,550 }, L"< ������� ����");
	startBut->onPress = []() {
		Scene::menu();
		return true;
	};
	NLOSpawner* inflator = new NLOSpawner(manager);
	manager->addObject(inflator, 2);
	manager->addObject(new Crossbow(
		[manager]() {
			Arrow* arrow = new Arrow([manager](float x, float y) {
				manager->pushGameEvent(Game::Event(x, y));
				});
			manager->addObject(arrow, 2);
			return arrow;
		}), 2);
	manager->addObject(startBut, 2);
}

void Scene::foreword() {
	GameManager* manager = GameManager::getThis();
	manager->clear(0b1);
	manager->addObject(new Monolog({ 20,20 },
		L"����������!\n������, � ��� ������������ ������������.\n�� ����, ������ �� ���, �� � ��� ���� 1 ������, ����� �� ���������.\n�����!",
		[manager]() {
			Button* nextB = new Button({ 350,290 }, L"������!");
			nextB->onPress = []() {
				Scene::timeGame();
				return true;
			};
			manager->addObject(nextB, 2);
		}), 2);
}

void Scene::missionComplete() {
	GameManager* manager = GameManager::getThis();
	manager->clear(0b1);

	manager->addObject(new Monolog({ 20,20 },
		L"�����!\n��� ������������.\n������� ��� �� ��������!",
		[manager]() {
			Button* nextB = new Button({ 350,290 }, L"< ������� ����");
			nextB->onPress = []() {
				Scene::menu();
				return true;
			};
			manager->addObject(nextB, 2);
		}), 2);
}

void Scene::missionFail() {
	GameManager* manager = GameManager::getThis();
	manager->clear(0b1);
	manager->addObject(new Monolog({ 20,20 },
		L"�� ���������!\n������� �� �� ���������.\n��������! =(",
		[manager]() {
			Button* nextB = new Button({ 350,290 }, L"< ������� ����");
			nextB->onPress = []() {
				Scene::menu();
				return true;
			};
			manager->addObject(nextB, 2);
		}), 2);
}

void Scene::youWin() {
	GameManager* manager = GameManager::getThis();
	manager->clear(0b1);
	manager->addObject(new Monolog({ 20,20 },
		L"������!\n��� ������ ��� ��� ������ � �����.\n� ����!",
		[manager]() {
			Button* nextB = new Button({ 350,290 }, L"< ������� ����");
			nextB->onPress = []() {
				Scene::menu();
				return true;
			};
			manager->addObject(nextB, 2);
		}), 2);
}