#include "Crossbow.h"
#include <iostream>

Crossbow::Crossbow(std::function<Arrow* ()> newArrow) :
	Object("Crossbow"),
	m_newArrow(newArrow),
	animation({ Animate::Start,2,90, 72,0 })
{
	if (!m_CrossbowTexture.loadFromFile("../assets/image/CROSSBOW.png")) {
		std::cout << "Asset not found!" << std::endl;
	}
	if (!m_BasketTexture.loadFromFile("../assets/image/Basket.png")) {
		std::cout << "Asset not found!" << std::endl;
	}


	m_CrossbowSprite.setTexture(m_CrossbowTexture);
	m_CrossbowSprite.setScale({ 4,4 });
	m_CrossbowSprite.setOrigin({ 45,55 });
	m_CrossbowSprite.setTextureRect(sf::IntRect(animation.width * 2, 0, animation.width, animation.hight));
	m_CrossbowSprite.setPosition({ 400,900 });

	m_BasketSprite.setTexture(m_BasketTexture);
	m_BasketSprite.setOrigin({ 0,290 });
	m_BasketSprite.setPosition({ 0,900 });
}

Crossbow::~Crossbow()
{
}

void Crossbow::draw(sf::RenderWindow& wd)
{
	sf::Vector2i pos = sf::Mouse::getPosition(wd);
	if (animation.anim == Animate::Start) {
		m_CrossbowSprite.move({ 0,-25 });
		m_BasketSprite.move({ 0,-25 });
		if (m_CrossbowSprite.getPosition().y <= 600) {
			animation.anim = Animate::NotLoad;
		}
	}
	else if (animation.anim == Animate::End) {
		m_CrossbowSprite.move({ 0,25 });
		m_BasketSprite.move({ 0,25 });
		if (m_CrossbowSprite.getPosition().y >= 900) {
			remove();
		}
	}
	else
	{
		m_CrossbowSprite.setPosition(sf::Vector2f(pos.x, 600));
	}
	wd.draw(m_BasketSprite);
	wd.draw(m_CrossbowSprite);
	if (m_arrow != NULL) {
		if (animation.anim == Animate::Attack) {
			if (animation.counter < 3) {
				m_CrossbowSprite.setTextureRect(sf::IntRect(animation.counter * animation.width, 0, animation.width, animation.hight));
				m_arrow->setPosition(pos.x, 450 - (10 * animation.counter));
				animation.counter++;
				return;
			}
			else {
				animation.anim = Animate::NotLoad;
				m_arrow->move(pos.y);
				m_arrow = NULL;
				return;
			}
		}
		m_arrow->setPosition(sf::Mouse::getPosition(wd).x);
	}

}

void Crossbow::eventUser(sf::Event& ev)
{
	if (ev.type == sf::Event::MouseButtonPressed) {
		if (animation.anim == Animate::Start || animation.anim == Animate::End) {
			return;
		}
		if (animation.anim == Animate::NotLoad)
		{
			if (m_arrow == NULL) {
				m_CrossbowSprite.setTextureRect(sf::IntRect(0, 0, animation.width, animation.hight));
				m_arrow = m_newArrow();
				m_arrow->setPosition(m_CrossbowSprite.getPosition().x, m_CrossbowSprite.getPosition().y);
			}
			animation.anim = Animate::Loaded;
		}
		else if (animation.anim == Animate::Loaded) {
			if (ev.mouseButton.y < 400) {
				animation.anim = Animate::Attack;
				animation.counter = 0;
			}
		}

	}
}

void Crossbow::onRemove()
{
	animation.anim = Animate::End;
}
