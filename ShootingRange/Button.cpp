#include <iostream>

#include "Button.h"
#include "Audio.h"

Button::Button(sf::Vector2f pos, const std::wstring title) :Object("Button"), m_pos(pos)
{
	if (!m_font.loadFromFile(static_cast<std::string>("../assets/Font/PixelFont.ttf"))) {
		std::cout << "Asset not found!" << std::endl;
	}

	m_text.setFont(m_font);
	m_text.setCharacterSize(20);
	m_text.setStyle(sf::Text::Style::Regular);
	m_text.setFillColor(color_Text);
	m_text.setString(title);
	m_text.setPosition(pos);
	m_text.setOrigin(-10, 0);
	sf::FloatRect rect = m_text.getGlobalBounds();

	m_background.setFillColor(color_None);
	m_background.setSize({ rect.width + 20, rect.height * 2 });
	m_background.setPosition(pos);
}


void Button::eventUser(sf::Event& ev)
{
	if (ev.type == sf::Event::MouseMoved) {
		if (m_background.getGlobalBounds().contains(sf::Vector2f(ev.mouseMove.x, ev.mouseMove.y))) {
			if (m_state != Button::State::Active) {
				m_state = Button::State::Focus;
				m_background.setFillColor(color_Focus);
			}
		}
		else if (m_state == Button::State::Focus) {
			m_state = Button::None;
			m_background.setFillColor(color_None);
		}
	}
	else if (ev.type == sf::Event::MouseButtonPressed) {
		if (m_background.getGlobalBounds().contains(sf::Vector2f(ev.mouseButton.x, ev.mouseButton.y))) {
			m_state = Button::State::Active;
			m_background.setFillColor(color_Active);
		}
	}
	else if (m_state == Button::State::Active && ev.type == sf::Event::MouseButtonReleased) {
		if (m_background.getGlobalBounds().contains(sf::Vector2f(ev.mouseButton.x, ev.mouseButton.y))) {
			m_state = Button::State::Focus;
			if (onPress != nullptr) {
				Audio::getThis()->play(Audio::Track::Click);
				if (onPress())
					onPress = NULL;
			}
			m_background.setFillColor(color_Focus);
		}
		else {
			m_state = Button::State::None;
			m_background.setFillColor(color_None);

		}
	}
}


void Button::draw(sf::RenderWindow& wd)
{
	wd.draw(m_background);
	wd.draw(m_text);
}

Button::~Button()
{
	std::cout << "~" << getName() << std::endl;
}
