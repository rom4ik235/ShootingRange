#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Engine.h"

int main()
{
	sf::RenderWindow* wd = new sf::RenderWindow(sf::VideoMode(800, 600),  L"Пострелушки", sf::Style::Default & ~sf::Style::Resize);
	Engine* eg = new Engine(*wd);
	eg->run(*wd);
	return EXIT_SUCCESS;
}