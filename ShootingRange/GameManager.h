#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <iostream>
#include <map>
#include <vector>
#include <queue> 
#include <mutex>

#include "Object.h"
#include "Event.hpp"

class GameManager
{
public:
	GameManager();
	~GameManager() { isLaunchedGame = false; thread.join(); };
	long int addObject(Object* obj, int layer = 0) {
		std::map<long int, Object*>& layerObjets = layers[layer];

		// old 
		//for (long int i = 0; true; i++) {
		//	if (layerObjets.find(i) == layerObjets.end()) {
		//		layerObjets[i] = obj;
		//		obj->setID(i);
		//		std::cout << "New Object[" << obj->getName() << "]\n";
		//		return i;
		//	}
		//}

		auto it = layerObjets.rbegin();
		long int id = it != layerObjets.rend() ? it->first + 1 : 0;
		obj->setID(id);
		layerObjets[id] = obj;
		return id;
	}

	void clear(char skip_layer = 0b0);

	void draw(sf::RenderWindow& wd);
	void eventUser(sf::Event& ev);
	void eventGame();

	void pushGameEvent(const Game::Event ev);

	GameManager(GameManager& other) = delete;
	void operator=(const GameManager&) = delete;

	static GameManager* getThis();

private:
	void cleaner();
	bool isLaunchedGame = true;
	std::thread thread;
	std::mutex mutex;
	sf::Sprite m_CursorSprite;
	sf::Texture m_CursorTexture;
	std::queue<Game::Event> gameEvents;
	std::queue<Object*> trash;

	std::map<long int, Object*> layers[3] = { std::map<long int, Object*>(), std::map<long int, Object*>(), std::map<long int, Object*>() };
};

#endif //GAMEMANAGER_H
