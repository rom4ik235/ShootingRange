#include "Scene.h"
#include <iostream>
#include <thread>
#include <ctime>

#include "GameManager.h"
#include "CloudBackground.h"
#include "Button.h"
#include "Ball.h"
#include "Crossbow.h"
#include "Arrow.h"

class BallInflator :public Object {
public:
	BallInflator(GameManager* manager) :
		Object("BallInflator"),
		manager(manager),
		thread(&BallInflator::run, this) {
	}
	~BallInflator() {
		isRepid = false;
		thread.join();
		std::cout << "~" << getName() << std::endl;
	}
private:
	void run() {
		std::srand(std::time(nullptr));
		while (isRepid) {
			Ball* ball = new Ball(sf::Vector2f(m_rand(800, 0), 700), m_rand(5, 1));
			manager->addObject(ball, 1);
			std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 7));
		}
	}

	float m_rand(const int max, const int min) {
		float value = (rand() % (max - min)) + 1;
		return value;
	}

	GameManager* manager;
	bool isRepid = true;
	std::thread thread;
};



void Scene::start() {
	GameManager* manager = GameManager::getThis();

	//BallInflator* inflator = ;
	manager->clear(0b1);
	manager->addObject(new BallInflator(manager), 2);
	Button* startBut = new Button({ 20,550 }, L"< ������� ����");
	startBut->onPress = []() {
		Scene::menu();
		return true;
	};
	manager->addObject(new Crossbow(
		[manager]() {
			Arrow* arrow = new Arrow([manager](float x, float y) {
				manager->pushGameEvent(Game::Event(x, y));
				});
			manager->addObject(arrow, 2);
			return arrow;
		}), 2);
	manager->addObject(startBut, 2);
}