#include "Engine.h"
#include "Scene.h"
#include "Audio.h"

Engine::Engine(sf::RenderWindow& wd) :m_wd(wd) {
	manager = GameManager::getThis();
	wd.setFramerateLimit(30);
	wd.setMouseCursorVisible(false);
}

void Engine::run(sf::RenderWindow& wd)
{
	Audio* Audio = Audio::getThis();
	Scene::fon();
	Scene::menu();
	while (wd.isOpen())
	{
		sf::Event event;
		while (wd.pollEvent(event))
		{
			manager->eventUser(event);
			if (event.type == sf::Event::Closed)
				wd.close();
		}

		manager->eventGame();
		wd.clear();
		manager->draw(wd);
		wd.display();
	}
}